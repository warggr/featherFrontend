# Feather frontend

A web application frontend for the lightweight user administration tool named "Feather".

## Project setup

```bash
yarn install
```

### Compiles and hot-reloads for development

```bash
yarn serve
```

This requires you to configure your development setup in a file `public/config.js`. See [public/config.js.dist](public/config.js.dist) for a reference.
The [Feather backend](https://gitlab.com/ingenieure-ohne-grenzen/feather) must be running and the configuration entry `API_BASE_URL` should point to the backend's base URL.

This requires you to configure your development setup in a file `public/config.js`. See [public/config.js.dist](public/config.js.dist) for a reference.
The [Feather backend](https://gitlab.com/ingenieure-ohne-grenzen/feather) must be running and the configuration entry `API_BASE_URL` should point to the backend's base URL.

### Compiles and minifies for production

```bash
yarn build
```

### Lints and fixes files

```bash
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## License

Licensed under [Apache 2.0](LICENSE)

## Contribution

Very welcome!

1. [Fork it](https://gitlab.com/ingenieure-ohne-grenzen/featherFrontend/-/forks/new)
1. Create your feature branch (git checkout -b my-new-feature)
1. Commit your changes (git commit -am 'Add some feature')
   - In your first commit, add your name and email address (if you like) to `AUTHORS.md`.
1. Push to the branch (git push origin my-new-feature)
1. Create a new Pull Request
