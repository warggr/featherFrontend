export type Alert = {
  type: "error" | "warning" | "success";
  uniqueType:
    | "network"
    | "unauthorized"
    | "forbidden"
    | "server_error"
    | "unexpected"
    | "unknown"
    | "custom";
  short: string;
  description: string;
};
