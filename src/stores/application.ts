import { defineStore } from "pinia";
import * as entities from "../entities";

const initialConfig: () => {
  config: entities.config.Config | undefined;
  session: entities.Session | undefined;
  alerts: entities.Alert[];
  logoutRequest: boolean;
} = () => ({
  config: undefined,
  session: undefined,
  alerts: [],
  logoutRequest: false,
});

export const useApplicationStore = defineStore("application", {
  state: initialConfig,
  actions: {
    setConfig(config: entities.config.Config) {
      this.config = config;
    },
    setSession(session: entities.Session) {
      this.session = session;
    },
    clearSession() {
      this.logoutRequest = false;
      this.session = undefined;
    },
    clearAlerts() {
      this.alerts = [];
    },
    pushAlert(alert: entities.Alert) {
      // Only push alert, if the same unique type is not here already
      // custom alerts will all be displayed
      if (alert.uniqueType !== "custom") {
        if (this.alerts.some((a) => a.uniqueType == alert.uniqueType)) {
          return;
        }
      }

      this.alerts.push(alert);
    },
    logoutRequested() {
      this.logoutRequest = true;
    },
  },
  getters: {
    getAlerts: (state) => {
      return state.alerts;
    },
  },
});
