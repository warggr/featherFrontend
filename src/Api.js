/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// eslint-disable-next-line no-undef
let apiBaseUrl = CONFIG.API_BASE_URL;

export default {
  defaultHandler: async function(vue, promise) {
    return await new Promise(resolve => {
      promise
        .then(data => {
          resolve(data);
        })
        .catch(reason => {
          switch (reason) {
            case "error.network": {
              vue.$emit("networkError");
              break;
            }
            case "error.forbidden": {
              vue.$emit("forbiddenError");
              break;
            }
            case "error.unexpected": {
              vue.$emit("unexpectedError");
              break;
            }
            case "error.unauthorized": {
              vue.$router.push("/logout").catch(error => {
                console.log(error);
              });
              break;
            }
            default: {
              console.log(reason);
            }
          }
        });
    });
  },
  getAllUsers: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/users", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                data.sort((o1, o2) => o1.username.localeCompare(o2.username));
                resolve(new Map(data.map(x => [x.id, x])));
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  changeNameOfUser: function(
    userId,
    displayName,
    firstname,
    surname,
    verified
  ) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/users/" + userId + "/name", {
        method: "POST",
        credentials: "include",
        body: JSON.stringify({ displayName, firstname, surname, verified }),
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  changeMailOfUser: function(userId, mail, verified) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/users/" + userId + "/mail", {
        method: "POST",
        credentials: "include",
        body: JSON.stringify({ mail, verified }),
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  changeUserStatus: function(userId, disabled) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/users/" + userId + "/status", {
        method: "POST",
        credentials: "include",
        body: JSON.stringify({ disabled }),
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  changePermissionsForUser: function(userId, permissionList) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/users/" + userId + "/permissions", {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(permissionList),
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  addUsersToGroup: function(groupId, userIdList, owner = false) {
    return new Promise((resolve, reject) => {
      fetch(
        apiBaseUrl + "/v1/groups/" + groupId + (owner ? "/owner" : "/member"),
        {
          method: "PUT",
          credentials: "include",
          body: JSON.stringify(userIdList),
        }
      )
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  removeUsersFromGroup: function(groupId, userId, owner = false) {
    return new Promise((resolve, reject) => {
      fetch(
        apiBaseUrl +
          "/v1/groups/" +
          groupId +
          (owner ? "/owner/" : "/member/") +
          userId,
        {
          method: "DELETE",
          credentials: "include",
        }
      )
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  addRelativeGroupToGroup: function(groupId, groupIdList, kind) {
    if (kind == "supergroup") {
      kind = "parents";
    } else if (kind == "subgroup") {
      kind = "children";
    } else if (kind == "ownergroup") {
      kind = "ownergroups";
    } else if (kind == "ownedgroup") {
      kind = "ownedgroups";
    }
    return new Promise((resolve, reject) => {
      if (
        !["parents", "children", "ownergroups", "ownedgroups"].includes(kind)
      ) {
        reject("error.invalidarguments");
      }
      fetch(apiBaseUrl + "/v1/groups/" + groupId + "/" + kind, {
        method: "PUT",
        credentials: "include",
        body: JSON.stringify(groupIdList),
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else if (response.status === 409) {
            reject("error.conflict");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  removeRelativeGroupFromGroup: function(groupId, relativeGroupId, kind) {
    if (kind == "supergroup") {
      kind = "parents";
    } else if (kind == "subgroup") {
      kind = "children";
    } else if (kind == "ownergroup") {
      kind = "ownergroups";
    } else if (kind == "ownedgroup") {
      kind = "ownedgroups";
    }
    return new Promise((resolve, reject) => {
      if (
        !["parents", "children", "ownergroups", "ownedgroups"].includes(kind)
      ) {
        reject("error.invalidarguments");
      }
      fetch(
        apiBaseUrl +
          "/v1/groups/" +
          groupId +
          "/" +
          kind +
          "/" +
          relativeGroupId,
        {
          method: "DELETE",
          credentials: "include",
        }
      )
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getAllGroups: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/groups", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                data.sort((o1, o2) => o1.name.localeCompare(o2.name));
                resolve(new Map(data.map(x => [x.id, x])));
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getGroupDetails: function(groupId) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/bindings/multiservice/groups/" + groupId, {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                data.groups.sort((o1, o2) => o1.name.localeCompare(o2.name));
                resolve({
                  groups: new Map(data.groups.map(x => [x.id, x])),
                  openProject: data.openProject,
                  nextcloud: data.nextcloud,
                });
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 403) {
            /* user does not have the rights to view details
             * (must be admin, or owner or member of the respective group) */
            reject("error.forbidden");
          } else if (response.status === 401) {
            // user is not logged in
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getNextcloudQuota: function(userId) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/bindings/nextcloud/" + userId + "/quota", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else if (response.status === 404) {
            reject("error.userNotFound");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getWorkPackageStats: function(userId) {
    return new Promise((resolve, reject) => {
      fetch(
        apiBaseUrl + "/v1/bindings/openproject/" + userId + "/work_packages",
        {
          method: "GET",
          credentials: "include",
        }
      )
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else if (response.status === 404) {
            reject("error.userNotFound");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getSession: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/session", {
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 404) {
            resolve(null);
          } else {
            console.log(response);
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getConfig: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/config")
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else {
            console.log(response);
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  ssoLogin: function(provider, code) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/authorizer/" + provider, {
        method: "POST",
        body: JSON.stringify(code),
        credentials: "include",
      })
        .then(response => {
          if (response.status === 201) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            resolve(null);
          } else {
            console.log(response);
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getAllValidInvitations: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/invitations", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getSandboxStatus: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/bindings/iog/sandbox", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getBackgroundJobStatus: function(jobId) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + `/v1/background_job/${jobId}`, {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getBackgroundJobResults: function(jobId) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + `/v1/background_job/${jobId}/results`, {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getSupportMembershipVerificationStatus: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/bindings/iog/users/verify", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            if (response.status === 200) {
              response
                .json()
                .then(data => {
                  resolve(data);
                })
                .catch(error => {
                  console.log(error);
                  reject("error.unexpected");
                });
            } else {
              reject("error.unexpected");
            }
          } else if (response.status === 403) {
            response
                .json()
                .then(data => {
                  resolve(data);
                })
                .catch(error => {
                  console.log(error);
                  reject("error.unexpected");
                });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else if (response.status === 400) {
            response
              .json()
              .then(data => {
                reject(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getGdprAcceptanceStatus: function(documentId) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + `/v1/gdpr/documents/` + documentId + "/status/me", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 404) {
            resolve();
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getGdprStatus: function(documentId) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + `/v1/gdpr/documents/` + documentId + "/status", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 404) {
            resolve();
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getGdpr: function(documentId, ignore400) {
    return new Promise((resolve, reject) => {
      const subDocument = documentId ? "/" + documentId : "";

      fetch(apiBaseUrl + `/v1/gdpr/documents` + subDocument, {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (
            response.status === 404 ||
            (ignore400 === true && response.status === 400)
          ) {
            resolve();
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  doAcceptGdpr: function(version) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + `/v1/gdpr/documents/` + version + "/accept", {
        method: "POST",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  saveGdpr: function(newForceDate, newValidDate, newGdprContent) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + `/v1/gdpr/documents`, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify({
          content: newGdprContent,
          forceDate: newForceDate,
          validDate: newValidDate,
        }),
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getInvitation: function(invitationId) {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/invitations/" + invitationId)
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 404) {
            resolve();
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getMyOpenActions: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/actions/my", {
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  getAllTrialRequests: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/bindings/iog/users/getAllTrialRequests", {
        method: "GET",
        credentials: "include",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                data.openRequests.sort((o1, o2) => o1.displayName.localeCompare(o2.displayName));
                resolve(data.openRequests);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else if (response.status === 401) {
            reject("error.unauthorized");
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
  finalizeMyActions: function() {
    return new Promise((resolve, reject) => {
      fetch(apiBaseUrl + "/v1/actions/my", {
        credentials: "include",
        method: "POST",
      })
        .then(response => {
          if (response.status === 200) {
            response
              .json()
              .then(data => {
                resolve(data);
              })
              .catch(error => {
                console.log(error);
                reject("error.unexpected");
              });
          } else {
            reject("error.unexpected");
          }
        })
        .catch(error => {
          console.log(error);
          reject("error.network");
        });
    });
  },
};
