module.exports = {
  root: true,
  env: {
    es2021: true
  },
  'extends': [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  rules: {
    'no-console': 'off',
  }
};
