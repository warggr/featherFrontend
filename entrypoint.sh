#!/bin/sh
set -e

# Substitute env variables
envsubst < /static/config.js.dist > /static/config.js

# Start nginx
exec "$@"
